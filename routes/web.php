<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\OfferController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\PostLikeController;
use App\Http\Controllers\StuffController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserProfileController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');


Route::group(['middleware' => ['auth']], function () {
    Route::resource('user', UserController::class);
    Route::get('user/{user}/delete', [UserController::class, 'destroy'])->name('user.delete');
});

Route::resource('post', PostController::class);

Route::get('user/{user:name}/profile', [UserProfileController::class, 'index'])->name('user.profile');

Route::get('post/{post}/delete', [PostController::class, 'destroy'])->name('post.delete');

Route::resource('offer', OfferController::class);

Route::resource('stuff', StuffController::class);

//Route::post('/post/{id}/likes', [PostLikeController::class, 'store'])->name('post.likes');
//Route::post('post/{post}/likes', [PostLikeController::class, 'destroy'])->name('post.delete');
Route::post('post/{post}/likes',[PostLikeController::class, 'store'])->name('post.likes');
Route::delete('post/{post}/likes',[PostLikeController::class, 'destroy'])->name('post.likes');

Route::resource('post/{post}/comment', CommentController::class);

//Route::post('post/{post}/comment',[CommentController::class, 'store'])->name('post.comment');


//Route::resource('offer', OfferController::class);




