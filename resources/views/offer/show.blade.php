@extends('layouts.app')

@section('content')
    <div class="container">


        <h1 style="text-align: center; margin-bottom: 20px">{{$offer->title}}</h1>


        <table class="table">
            <thead>
            <tr>
                <th scope="col">Palivo</th>
                <td>{{$offer->palivo}}</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Najazdene</th>
                <td>{{$offer->najazdene}}</td>
            </tr>
            <tr>
                <th scope="row">Vykon</th>
                <td>{{$offer->vykon}}</td>
            </tr>
            <tr>
                <th scope="row">Prevodovka</th>
                <td>{{$offer->prevodovka}}</td>
            </tr>
            <tr>
                <th scope="row">Farba</th>
                <td>{{$offer->farba}}</td>
            </tr>
            <tr>
                <th scope="row">Cena</th>
                <td>{{$offer->cena}}€</td>
            </tr>
            </tbody>
        </table>

    </div>
@endsection
