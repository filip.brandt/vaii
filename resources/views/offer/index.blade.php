@extends('layouts.app')

@section('content')
    <div class="container">
        @if(count($offers))
            @foreach($offers as $offer)


                <div class="card mb-3">

                    <img src="{{$offer->img}}" >
                    <div class="card-body">
                        <a href="{{ route('offer.show', $offer->id) }}" style="font-size: 40px; color: black" >{{$offer->title}}</a>
                    </div>
                </div>

            @endforeach
        @else
            There are no offers
        @endif
    </div>
@endsection
