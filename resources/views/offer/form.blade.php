<div class="form-group text-danger">
    @foreach($errors->all() as $error)
        {{$error}} <br>
    @endforeach
</div>

<form method="post" action="{{ $action }}">
    @csrf
    @method($method)
    <div class="form-group">
        <label for="name">Full name</label>
        <input type="text" class="form-control" id="body" name="body" placeholder="Full name" value="{{ old('body', @$model->body) }}"
        >
    </div>

    <div class="form-group">
        <input type="submit" class="btn-primary form-control">
    </div>
</form>
