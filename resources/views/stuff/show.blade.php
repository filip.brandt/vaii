<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @yield('title')
    </title>

    <link rel="stylesheet" href="{{ asset('css/stuff/show.css') }}">

    <!-- Additional per-page css -->
    @yield('css')
</head>

<body>

@yield('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="js/script.js"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<!-- Include per-page JS -->
@yield('js')
</body>

</html>

@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="info">
            <img src="{{$stuff->img}}">

            <h1 style="margin-bottom: 30px">{{$stuff->title}}</h1>
            <p style="margin-bottom: 30px">{{$stuff->body}}</p>
        </div>

        <table class="table">
            <thead>
            <tr>
                <th scope="col">PSC</th>
                <td>{{$stuff->price}}</td>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Cena</th>
                <td>{{$stuff->psc}}€</td>
            </tr>
            <tr>
                <th scope="row">Autor</th>
                <td><a href="{{route('user.profile', $stuff->user)}}">{{$stuff->user->name}}</a></td>
            </tr>
        </table>

    </div>

@endsection
