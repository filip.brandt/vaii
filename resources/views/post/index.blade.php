<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @yield('title')
    </title>

    <link rel="stylesheet" href="{{ asset('css/post/index.css') }}">

    <!-- Additional per-page css -->
    @yield('css')
</head>

<body>

@yield('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="js/script.js"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<!-- Include per-page JS -->
@yield('js')
</body>

</html>

@extends('layouts.app')

@section('content')
    <div class="container">

        @if((auth()->user()->name) == 'admin')
            <div class="mb-4">
                <a href="{{ route('post.create') }}" class="btn btn-success" role="button">Pridat novy príspevokt</a>
            </div>
        @endif
    </div>
    @if(count($posts))
        @foreach($posts as $post)
            <div class="container">
                <div class="card text-white mb-3">
                    <img class="card-img"
                         src="https://scontent-vie1-1.xx.fbcdn.net/v/t1.0-9/139832331_2744794662426834_5937522858995968340_o.jpg?_nc_cat=100&ccb=2&_nc_sid=730e14&_nc_ohc=cb3YylgKI-UAX-o2eSy&_nc_ht=scontent-vie1-1.xx&oh=e9e95f08a92baa16886ed0f133a60333&oe=602A6DE9"
                         alt="Card image">
                    <div class="card-img-overlay">
                        <p class="date">{{$post->created_at->diffForHumans()}}</p>
                        <a href="{{ route('post.show', $post->id) }}" class="odkaz text-white"
                           title="Show">{{$post->title}}</a>
                        {{--                        <h1 class="card-title">{{$post->title}}</h1>--}}
{{--                        <p class="card-text">{{$post->body}}</p>--}}
                        <div class="like" id="wrapper">
                            <div class="likeButton" id="div1">
                                @if(!$post->liked(auth()->user()))
                                    <div id="first"></div>
                                    <form action="{{ route('post.likes', $post) }}" method="post">
                                        @csrf
                                        <button type="submit" class="btn btn-outline-success">Páči sa mi</button>
                                    </form>
                                @else
                                    <form action="{{ route('post.likes', $post) }}" method="post">
                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-outline-danger">Nepáči sa mi</button>
                                    </form>
                                @endif
                            </div>
                            <div class="likePocet" id="div2">
                                {{$post->likes->count()}} {{Str::plural('like',$post->likes->count())}}
                            </div>
                        </div>

                        {{--     <span>{{$post->likes->count()}} {{Str::plural('like',$post->likes->count())}} likes </span>--}}
                        {{--                        <p class="date">Last updated 3 mins ago</p>--}}
                    </div>
                </div>
                @if($post->adminPermission(auth()->user()))
                    <div>
                        <div class="tlacidlo" id="button1">
                            <form action="{{ route('post.destroy', $post) }}" method="post">
                                @csrf
                                @method('DELETE')
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </div>
                        <div class="tlacidlo" id="button2">
                            <a href="{{ route('post.edit', $post) }}" title="Edit" class="btn btn btn-primary">Edit</a>
                        </div>
                    </div>
                @endif
            </div>
        @endforeach
    @else
        There are no posts
        @endif
        </div>
@endsection

