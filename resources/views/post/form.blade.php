<div class="form-group text-danger">
    @foreach($errors->all() as $error)
        {{$error}} <br>
    @endforeach
</div>

<form method="post" action="{{ $action }}">
    @csrf
    @method($method)
    <div class="form-group">
        <label for="name">Titulok</label>
        <input type="text" class="form-control" id="title" name="title" value="{{ old('body', @$model->title) }}"
        >
    </div>

    <div class="form-group">
        <label for="exampleFormControlTextarea1">Popis</label>
        <textarea class="form-control" id="body" name="body" rows="3">{{ old('body', @$model->body) }}</textarea>
    </div>




    <div class="form-group">
        <input type="submit" class="btn-primary form-control">
    </div>
</form>
