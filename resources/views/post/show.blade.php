<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>
        @yield('title')
    </title>

    <link rel="stylesheet" href="{{ asset('css/post/show.css') }}">

    <!-- Additional per-page css -->
    @yield('css')
</head>

<body>

@yield('content')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="js/script.js"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>

<!-- Include per-page JS -->
@yield('js')
</body>

</html>

@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="image">
            <img src="images/bmw_post.jpg">
        </div>

        <h1>{{$post->title}}</h1>
        <p>{{$post->body}}</p>
    </div>

@endsection

