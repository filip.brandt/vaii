<?php

namespace App\Http\Controllers;

use App\Models\Stuff;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class StuffController extends Controller
{
    public function index()
    {
        $stuffs = Stuff::all();
        return view('stuff.index')->with('stuffs', $stuffs);
    }

    public function create()
    {
        return view('stuff.create', [
            'action' => route('stuff.store'),
            'method' => 'post'
        ]);
    }

    public function store(Request $request)
    {

        $request->validate( [
            'body' => ['required', 'string'],
            'title' => ['required', 'string',  'digits_between:10,100'],
            'psc' => ['required',  'min:5', 'max:5'],
            'price' => ['required', 'digits_between:1,10'],
        ]);

        $request->user()->stuff()->create([
            'body'=>$request->body
        ]);
//        $stuff->save();
        return redirect()->route('stuff.index');
    }

    public function show(Stuff $stuff)
    {
//        $post = Post::find($id);
        return view('stuff.show')->with('stuff', $stuff);
    }


}
