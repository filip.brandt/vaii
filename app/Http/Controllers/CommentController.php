<?php

namespace App\Http\Controllers;

use App\Models\Post;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function index(){

    }

    public function create(Post $post)
    {
        return view('post.show', [
            'action' => route('comment.store'),
            'method' => 'post'
        ]);
    }

    public function store(Request $request)
    {
//        $stuff = Stuff::create($request->all());
//        $stuff->user_id = Auth::user()->id;
        $request->user()->stuff()->create([
            'body'=>$request->body
        ]);
//        $stuff->save();
        return redirect()->route('post.show');
    }
}
