<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserProfileController extends Controller
{
    public function index(User $user){
//        dd($user);
        $stuffs = $user->stuff()->with(['users','stuff']);

        return view('user.profile.index',[
            'user' => $user,
            'stuffs' => $stuffs
        ]);
    }
}
