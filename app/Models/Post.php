<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'body'
    ];

    public function adminPermission(User $user)
    {
        return $user->name == 'admin';
    }

    public function likes(){
        return $this->hasMany(Like::class);
    }

    public function liked(User $user){
        return $this->likes->contains('user_id', $user->id);
    }
}
